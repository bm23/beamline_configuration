//////////////////////////////////////////////
//
// Multiplexeur Program BM23 opiom
//
//////////////////////////////////////////////

// INPUT
wire musst_outa;
wire musst_outb;
wire p201_gate;

// OUTPUT
reg p201_trig;
reg fx_trig;
reg fx_gate;

// Selector
wire sel_trig;
wire fx_ctrl;

// Input/Output Assignment
assign musst_outa = I1; // Input from MUSST OUTA
assign musst_outb = I2; // Input from MUSST OUTB
assign p201_gate  = I3; // Input from P201 Gate Out (Channel10)

assign O1 = musst_outa; // copy of MUSST OUTA
assign O2 = musst_outb; // copy of MUSST OUTB
assign O3 = p201_gate;  // copy of P201 Gate Out (channel10)
assign O4 = p201_trig;  // ZAP Trig p201 (channel9)
assign O5 = fx_trig;    // ZAP Trig Fx ZAP
assign O6 = fx_gate;    // STEP p201 gate to FalconX

// Register Assignement
assign self_trig = IM1;    // ZAP selector

// ZAP selector
always @(sel_trig or musst_outa or p201_gate)
begin
  case (self_trig)
      2'b1 : begin
                p201_trig = musst_outa;
                fx_trig = musst_outa;
                fx_gate = 0;
              end

      default : begin
                  p201_trig = 0;
                  fx_trig = 0;
                  fx_gate = p201_gate;
                end
  endcase
end


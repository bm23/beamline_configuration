
from bliss.setup_globals import *

print("")
print("Welcome to your new 'session_heiden' BLISS session !! ")
print("")
print("You can now customize your 'session_heiden' session by changing files:")
print("   * /session_heiden_setup.py ")
print("   * /session_heiden.yml ")
print("   * /scripts/session_heiden.py ")
print("")


import numpy as np
import math

from bliss.setup_globals import *
from bliss.common.plot import plot

from bm23.bm23plot import *


load_script("session_silx.py")

print("")
print("Welcome to your new 'session_silx' BLISS session !! ")
print("")
print("You can now customize your 'session_silx' session by changing files:")
print("   * /session_silx_setup.py ")
print("   * /session_silx.yml ")
print("   * /scripts/session_silx.py ")
print("")

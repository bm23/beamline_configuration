
from bliss.setup_globals import *

load_script("kirill.py")

print("")
print("Welcome to your new 'kirill' BLISS session !! ")
print("")
print("You can now customize your 'kirill' session by changing files:")
print("   * /kirill_setup.py ")
print("   * /kirill.yml ")
print("   * /scripts/kirill.py ")
print("")
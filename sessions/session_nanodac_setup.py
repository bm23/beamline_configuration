
from bliss.setup_globals import *

load_script("session_nanodac.py")

print("")
print("Welcome to your new 'session_nanodac' BLISS session !! ")
print("")
print("You can now customize your 'session_nanodac' session by changing files:")
print("   * /session_nanodac_setup.py ")
print("   * /session_nanodac.yml ")
print("   * /scripts/session_nanodac.py ")
print("")
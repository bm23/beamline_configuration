
from bliss.setup_globals import *

load_script("linus.py")

print("")
print("Welcome to your new 'linus' BLISS session !! ")
print("")
print("You can now customize your 'linus' session by changing files:")
print("   * /linus_setup.py ")
print("   * /linus.yml ")
print("   * /scripts/linus.py ")
print("")

from bliss.setup_globals import *

from bm23.BM23utils import *

spectro_energy = spectro.energy_axis
spectro_bragg = spectro.bragg_axis

det0_bragg = spectro.detector.bragg_axis
det0_energy = spectro.detector.energy_axis

anm2_bragg = spectro.analysers.anm2.bragg_axis
anm2_energy = spectro.analysers.anm2.energy_axis

anm1_bragg = spectro.analysers.anm1.bragg_axis
anm1_energy = spectro.analysers.anm1.energy_axis

an0_bragg = spectro.analysers.an0.bragg_axis
an0_energy = spectro.analysers.an0.energy_axis

anp1_bragg = spectro.analysers.anp1.bragg_axis
anp1_energy = spectro.analysers.anp1.energy_axis

anp2_bragg = spectro.analysers.anp2.bragg_axis
anp2_energy = spectro.analysers.anp2.energy_axis

